# Engineering Logbook 12

|Name|Matric No|Subsytems|Logbook|Date|
|-----|-----|-----|-----|-----|
|Che Wan Muhammad Luqman bin Che Wan Mohamad Anan|197589|Flight System Integration|12|21/01/2021|

|Agenda|
|-----|
| Test the current system when the gondola attached to the airship. |


|Goals|
|-----|
| To check the stability and also helping the design and structure team to conduct their test on the strusture arm and gondola. |


|Method and Justification|
|-----|
| The thruster arm was attached to the airship, the extension wire were connect with the control board in the gondola and the servo was tested. |

|Impact on the project|
|-----|
| From the test, the thruster arm able to check wheter their current design is stable and strong to support the motor when flying. |


|Next Step|
|-----|
| Repeat the testing as the control program is being updating by Azizi. |
