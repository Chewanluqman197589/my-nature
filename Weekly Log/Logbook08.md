# Engineering Logbook 08

|Name|Matric No|Subsytems|Logbook|Date|
|-----|-----|-----|-----|-----|
|Che Wan Muhammad Luqman bin Che Wan Mohamad Anan|197589|Flight System Integration|08|24/12/2021|

|Agenda|
|-----|
| Soldering the wire from esc with banana connecter. |


|Goals|
|-----|
| Make a new connecter for the battery |


|Method and Justification|
|-----|
| The given Lipo Battery connecter were small, hence a new connecter for the battery was made. The banana connecter and heat shrink were insufficient, thus we buy the banana connecter and heat shrink. |

|Impact on the project|
|-----|
| The current Lipo Battery can connect into the board. The connection from ESC to motor can be done. |


|Next Step|
|-----|
| Soldering the remaining part of the connection. |
