# Engineering Logbook 01

|Name|Matric No|Subsytems|Logbook|Date|
|-----|-----|-----|-----|-----|
|Che Wan Muhammad Luqman bin Che Wan Mohamad Anan|197589|Flight System Integration|01|28/10/2021|

|Agenda|
|-----|
| Create a gantt chart for the flight system intergration subsystem based on the task given by Fikri. |


|Goals|
|-----|
| To have a clear and align progress with other subsystems. |


|Method and Justification|
|-----|
| By observing other subsytems gantt chart, we align the time and the task that were given in order for project's progress is moving smoothly with other subsystems. |

|Impact on the project|
|-----|
| Since our group is the group that need to intergrate the entire project, our gantt chart is basically the guide for other to recognize the overall flow of the project, so that other subsystems can follow up with other subsystems.  |


|Next Step|
|-----|
| Presenting the gantt chart to everyone, update the logbook, check the work done for the previous task on gitlab. |
