# Engineering Logbook 05

|Name|Matric No|Subsytems|Logbook|Date|
|-----|-----|-----|-----|-----|
|Che Wan Muhammad Luqman bin Che Wan Mohamad Anan|197589|Flight System Integration|05|26/11/2021|

|Agenda|
|-----|
| Present the table that Dr. Salah ask to gather last week. Monitor other subsytems progress and the equipment tracker. |


|Goals|
|-----|
| To ensure that the progress of each group is align with the gantt chart proposed and to present the task given. |


|Method and Justification|
|-----|
| Ask the available paramater from the simulation, propulsion and design team as they obatained or measured the parameters due to their task. |

|Impact on the project|
|-----|
| Hence, we able to estimate the theoretical performance of the HUA. |


|Next Step|
|-----|
| To be continue... |
