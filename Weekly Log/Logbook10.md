# Engineering Logbook 10

|Name|Matric No|Subsytems|Logbook|Date|
|-----|-----|-----|-----|-----|
|Che Wan Muhammad Luqman bin Che Wan Mohamad Anan|197589|Flight System Integration|10|07/01/2022|

|Agenda|
|-----|
| Buy the insufficient wire. Finish the remaining part of the soldering extension wire. |


|Goals|
|-----|
| Finish the connection between the motor and main control box. |


|Method and Justification|
|-----|
| We buy Wire Multicore AWG 14 due Wire Multicore AWG 16 were sold out. |

|Impact on the project|
|-----|
| The reason Wire AWG 14 were being choose is because it can resist the same and more load as AWG 16. |


|Next Step|
|-----|
| Assembly and test the airship if possible. |
