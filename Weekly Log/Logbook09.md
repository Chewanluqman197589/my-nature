# Engineering Logbook 09

|Name|Matric No|Subsytems|Logbook|Date|
|-----|-----|-----|-----|-----|
|Che Wan Muhammad Luqman bin Che Wan Mohamad Anan|197589|Flight System Integration|09|31/12/2021|

|Agenda|
|-----|
| Soldering the remaining part of the connection |


|Goals|
|-----|
| Determined the connectivity of the component on the board. |


|Method and Justification|
|-----|
| Since the extension wire were not enough, we buy the remaining wire |

|Impact on the project|
|-----|
| The Control Group can done the testing PSHAU firmware. |


|Next Step|
|-----|
| Purchase the inadequate wire. Complete the remaining soldering. |
