# Engineering Logbook 03

|Name|Matric No|Subsytems|Logbook|Date|
|-----|-----|-----|-----|-----|
|Che Wan Muhammad Luqman bin Che Wan Mohamad Anan|197589|Flight System Integration|03|12/11/2021|

|Agenda|
|-----|
| Monitor the progress of the project and other subsystems, update the progress and help the other subsystems if needed. Find the CG of the airship without the payload. |


|Goals|
|-----|
| To secure that the progress is align with the gantt charts. To obtained the CG of the airship without payload. |


|Method and Justification|
|-----|
| I asked my small group for their subsystems current progress based on the task given and their gantt chart. Check the current progress whether it is align or not with the gantt chart. Some of our group members helps propulsion team to conduct a little test to obtain the thrust of the airship to move foward.  |

|Impact on the project|
|-----|
| By checking the status of each subsystem, we able to checked whether the status is aligned or not with the proposed gantt charts in order to have a clear view of the project. |


|Next Step|
|-----|
| Monitor the progress of other subsystems group. |
