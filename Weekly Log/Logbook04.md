# Engineering Logbook 04

|Name|Matric No|Subsytems|Logbook|Date|
|-----|-----|-----|-----|-----|
|Che Wan Muhammad Luqman bin Che Wan Mohamad Anan|197589|Flight System Integration|04|19/11/2021|

|Agenda|
|-----|
| Monitor other subsytems group progress and check whether they still on track or not |


|Goals|
|-----|
| To align the progress of the work as supposed in the gantt chart for a smooth project progress. |


|Method and Justification|
|-----|
| Check the current progress of each subsytems and compare to the proposed gantt chart. By doing this, we able to track the progress carefully and make sure whether it align or not with the progress of the project. |

|Impact on the project|
|-----|
| Having all of the team on the track will greatly boost the progress of the project. |


|Next Step|
|-----|
| Find the CG of the aircraft. Perform the integration for small HAU. Monitor the work of other subsystems. |
