# Engineering Logbook 11

|Name|Matric No|Subsytems|Logbook|Date|
|-----|-----|-----|-----|-----|
|Che Wan Muhammad Luqman bin Che Wan Mohamad Anan|197589|Flight System Integration|11|14/01/2022|

|Agenda|
|-----|
| Soldering the wire, connectiong the servo to the board, test the control system. Measure and cutting the extension wires.|


|Goals|
|-----|
| To finallize the control system and check the connectivity between the survos, motors and the board. |


|Method and Justification|
|-----|
| The soldering of the wire were done by haikal and zarif, while I measure the extension wire. I was helping Zarif, Aliff and Haikal in doing their task. Mostly just run errands. |

|Impact on the project|
|-----|
| The control subsystems able to determined the number of servo and the functionality of the components. |


|Next Step|
|-----|
| Attach the assembled board and servo onto the airship for testing. |
