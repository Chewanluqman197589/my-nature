# Engineering Logbook 07

|Name|Matric No|Subsytems|Logbook|Date|
|-----|-----|-----|-----|-----|
|Che Wan Muhammad Luqman bin Che Wan Mohamad Anan|197589|Flight System Integration|07|17/12/2021|

|Agenda|
|-----|
| Choose and decide which template that should be choose for the academic report. Make a checklist for operation |


|Goals|
|-----|
| Finalised the template and flow of the academic journal article. |


|Method and Justification|
|-----|
| Search and compare some of the academic reports for the references and guides in order to choose the perfect the template for this project's academic journal article. |

|Impact on the project|
|-----|
| Deciding the template of the academic journal article is just for the documentation of this project. Since our group need to write the academic journal article/report for this project, we decide to pick a formal and suitable template that is simple but give big impact so that the readers and other people who read the journal will understand clearly on what our project is, how it is conducted and the resulted obtained. |


|Next Step|
|-----|
| Intergrate the component that has been solder by Fikri and update the academic journal article/report. |
