# Engineering Logbook 13

|Name|Matric No|Subsytems|Logbook|Date|
|-----|-----|-----|-----|-----|
|Che Wan Muhammad Luqman bin Che Wan Mohamad Anan|197589|Flight System Integration|13|28/01/2022|

|Agenda|
|-----|
| Retesting the control system, the finalize of the checklist. |


|Goals|
|-----|
| To able the HUA fly on the flying day. |


|Method and Justification|
|-----|
| Since it is the last week and it the week of the airship would fly, the test of the control sytems were done several times as to confirm the movement and control of the motors and servos. The night before the day, the system was checked and in a good condition but during the fly in the morning, there are several problems occure as on of the servo did not function properly and there is an accident that cause the thruster arm to bend and hit the airship which create a hole on the airship body. |

|Impact on the project|
|-----|
| - |


|Next Step|
|-----|
| Update the Logbook, prepare the report and resume |
